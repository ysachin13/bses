$(function () {

	$('#edit').click(() => {
		$('.editable').addClass('edit').attr('contenteditable','true');
	});

	$('#saveData').click(() => {
		$('.editable').removeClass('edit').removeAttr('contenteditable');
	});

	// Store data in localStorage
	let $name = document.getElementById('name'),
			$mobile = document.getElementById('mobile'),
			$address = document.getElementById('address'),
			$email = document.getElementById('email');

	let accountEdit = ($lacalVar, $valueSelector) => {
		$($valueSelector).blur( () => {
			localStorage.setItem($lacalVar, $valueSelector.innerHTML);
		});

		if (localStorage.getItem($lacalVar)) {
			$valueSelector.innerHTML = localStorage.getItem($lacalVar);
		}
	}

	accountEdit ('name', $name);
	accountEdit ('mobile', $mobile);
	accountEdit ('address', $address);
	accountEdit ('email', $email);

	// Google Map
	if (navigator.geolocation) {
 		navigator.geolocation.getCurrentPosition(function(position) {

	    // Get the coordinates of the current possition.
	    var lat = position.coords.latitude;
	    var lng = position.coords.longitude;

	    // Create a new map and place a marker at the device location.
	    var map = new GMaps({
	      el: '#map',
	      lat: lat,
	      lng: lng
	    });

	    map.addMarker({
	      lat: lat,
	      lng: lng
	    });

	  });
  } else {
 		console.log('Geolocation is not supported by this browser');
  }

  // Form validation
	function saveChanges() {
		debugger;
		var mail = document.getElementById("email").value;
	  var filter = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
	  var phoneNum = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		var phone = document.getElementById("phone").value;
		var sms = document.getElementById("sms").value;

		if(mail === ""){
			document.getElementById('val-email').innerHTML = 'Enter Email Id';
		}else if(!filter.test(mail) && mail!= "") {
			document.getElementById('val-email').innerHTML = 'Enter valid Email Id';
		}

		if(phone === ""){
			document.getElementById('val-phone').innerHTML = 'Enter phone number';
		}else if(!phoneNum.test(phone) && phone!= "" && phone.length!==10) {
			document.getElementById('val-phone').innerHTML = 'Enter valid phone number';
		}

	}

	// Make navigtion Active
  var href = location.href,
  		pgurl = href.substr(href.lastIndexOf('/') + 1);
		$('a[href="' + pgurl + '"]').addClass('active');

	// Datetimepicker initialization
	$('#datetimepicker1').datetimepicker();

	// Line chart
	var options = {
	  type: 'line',
	  data: {
	    labels: ["Sep 2015", "Oct 2015", "Nov 2015", "Dec 2015", "Jan 2016", "Feb 2016"],
	    datasets: [
		    {
		      label: '',
		      data: [12, 19, 3, 80, 2, 3],
	      	borderWidth: 1
	    	}
			]
	  },
	  options: {
	  	scales: {
	    	yAxes: [{
	        ticks: {
	        	max: 150,
	        	stepSize: 50,
						callback: function(value, index, values) {
              return '$' + value;
            }
	        }
	      }]
	    }
	  }
	}

	var ctx = document.getElementById('chartJSContainer').getContext('2d');
	new Chart(ctx, options);

});